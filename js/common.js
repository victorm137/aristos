/**
 * Table-slider
 */

var slider_counter = 1;

showTable();


$(document).ready(function(){

    $("#show-hide-1").click(function(){
        $("#answer-1").toggle();
        ($("#show-hide-1").text() === "Свернуть ответ >>") ? $("#show-hide-1").text("Показать ответ >>") : $("#show-hide-1").text("Свернуть ответ >>");
    });

    $("#show-hide-2").click(function(){
        $("#answer-2").toggle();
        ($("#show-hide-2").text() === "Свернуть ответ >>") ? $("#show-hide-2").text("Показать ответ >>") : $("#show-hide-2").text("Свернуть ответ >>");
    });

    $("#show-hide-3").click(function(){
        $("#answer-3").toggle();
        ($("#show-hide-3").text() === "Свернуть ответ >>") ? $("#show-hide-3").text("Показать ответ >>") : $("#show-hide-3").text("Свернуть ответ >>");
    });

    $("#show-hide-4").click(function(){
        $("#answer-4").toggle();
        ($("#show-hide-4").text() === "Свернуть ответ >>") ? $("#show-hide-4").text("Показать ответ >>") : $("#show-hide-4").text("Свернуть ответ >>");
    });

    $("#show-hide-5").click(function(){
        $("#answer-5").toggle();
        ($("#show-hide-5").text() === "Свернуть ответ >>") ? $("#show-hide-5").text("Показать ответ >>") : $("#show-hide-5").text("Свернуть ответ >>");
    });


    $(".arrow-right").click(function(){
        slider_counter = slider_counter++ % 3 + 1;

        showTable();
    });


    $(".arrow-left").click(function(){

        switch (slider_counter) {
            case 1:
            default:
                slider_counter = 3;
                break;
            case 3:
                slider_counter = 2;
                break;
            case 2:
                slider_counter = 1;
                break;
        }

        showTable();
    });

});


function showTable() {

    $(".table-1").css("display","none");
    $(".table-2").css("display","none");
    $(".table-3").css("display","none");

    $(".table-" + slider_counter.toString()).css("display","block");

}
